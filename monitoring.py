import os
import time
import argparse
from prometheus_client import start_http_server, Counter
from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY

parser = argparse.ArgumentParser()
parser.add_argument('--root', metavar='rootdir', type=str, default='/',
                    help='system root directory')
parser.add_argument('--port', metavar='port', type=int, default=8000,
                    help='system root directory')
pargs = parser.parse_args()

class NetworkStatCollector(object):
    prefix = 'netexporter'

    def collect(self):
        path = os.path.join(pargs.root, 'sys', 'class', 'net')
        cf = CounterMetricFamily(f'{self.prefix}_interface', f'Network stats for interface', labels=['interface', 'type'])
        for dir in next(os.walk(path))[1]:
            self.CollectInterface(path, dir, cf)
        yield cf

    def CollectInterface(self, root: str, iface: str, family: CounterMetricFamily):
        path = os.path.join(root, iface, 'statistics')
        for stat in next(os.walk(path))[2]:
            try:
                with open(os.path.join(path, stat), mode='r') as f:
                    data = f.read()
                    family.add_metric([iface, stat], int(data))
            except:
                pass

REGISTRY.register(NetworkStatCollector())

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(pargs.port)
    # sleep indefenetly
    while True:
        time.sleep(100)
