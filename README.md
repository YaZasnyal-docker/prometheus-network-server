# Usage

```yaml
version: '3.4'

services:
  pitemp:
    image: registry.gitlab.com/yazasnyal-docker/prometheus-network-server:latest
    command:
      - '--root=/host'
    ports:
      - 8000:8000
    volumes:
      - '/:/host:ro,rslave'
```
