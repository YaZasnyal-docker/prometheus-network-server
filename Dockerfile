FROM python:3-slim
WORKDIR /app
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8000/tcp
ENTRYPOINT ["python3", "./monitoring.py"]
CMD ["--root=/host"]
